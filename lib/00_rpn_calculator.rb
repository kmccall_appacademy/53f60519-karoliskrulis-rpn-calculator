class RPNCalculator
  # TODO: your code goes here!

  def initialize
    @stack = []
  end

  def push(el)
    @stack << el
  end

  def plus
    operate(:+)
  end

  def minus
    operate(:-)
  end

  def times
    operate(:*)
  end

  def divide
    operate(:/)
  end

  def value
    @stack.last
  end

  def tokens(string)
    items = string.split(' ')
    items.map do |el|
      if ["+", "-", "/", "*"].include?(el)
        el.to_sym
      else
        el.to_i
      end
    end
  end

  def evaluate(string)
    tokens(string).each do |el|
      if el.is_a? Integer
        self.push(el)
      elsif el.is_a? Symbol
        self.operate(el)
      end
    end
    value
  end

  def operate(operator)
    raise "calculator is empty" if @stack.count <= 1

    first_number = @stack.pop
    second_number = @stack.pop

    if operator == :+
      @stack << first_number + second_number
    elsif operator == :-
      @stack << second_number - first_number
    elsif operator == :/
      @stack << second_number.fdiv(first_number)
    elsif operator == :*
      @stack << second_number * first_number
    end
  end

end
